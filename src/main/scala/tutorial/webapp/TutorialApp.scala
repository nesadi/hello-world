package tutorial.webapp
import org.scalajs.dom
import dom.document
import org.querki.jquery._

import scala.scalajs.js.annotation.{JSExportAll, JSExportTopLevel}

@JSExportTopLevel(name="TutorialApp")@JSExportAll
object TutorialApp {
  def main(args: Array[String]): Unit = {
    $("body").append("<p>Hello world!</p>")
  }

  def addClickedMessage(): Unit = {
    $("body").append("<p>You clicked the button ... </p>")
  }
}

